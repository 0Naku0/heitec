+++
title = "Py-KMS"
date = "2022-02-20T17:48:36+01:00"
author = "Daniel"
authorTwitter = "" #do not include @
cover = ""
tags = ["linux", "windows"]
keywords = ["", ""]
description = "Linux KMS Server"
showFullContent = false
readingTime = false
+++

## Windows KMS

KMS aktiverung unter Microsoft für Windows und/oder Office ist schon eine recht praktische Sache. Wenn der KMS Server einmal eingerichtet ist, muss man sich über Lizenzkeys und aktivierung der Geräte kaum mehr Gedanken machen, sondern die werden einfach automatisch aktiviert.

Man muss natürlich trotzdem die nötigen Client Lizenzen besitzen und auch bei einer Prüfung vorweisen können, aber man spart sich einfach das ganze verwalten der einzelnen Clients und auch keiner kann einem einfach so die Lizenz auf dem PC auslesen und gegenfalls sogar dann im Internet veröffentlichen.

Besonders z.B. in Schulen oder großen Firmen, wo öfter mal PCs automatisiert neu installiert werden ist das ein Segen.

Problem nun bei der Sache ist, das der offizielle MS KMS Server ein Windows Server Dienst ist und eigentlich deswegen auch auf einem Windows Server installiert werden sollte, der im Besten Fall sogar AD integriert sein sollte.

Man kann den KMS Dienst auch ohne AD, als Standalone Dienst betreiben, dann aktiviert er Windows aber erst nach 25 eindeutigen Anfragen von Windows Clients (geklonte PCs zählen nur als ein Gerät, wenn deren ID nicht speziell vor dem aufspielen resetet wird). Der Standalone Dienst läuft offiziell von MS zwar auch auf einem Windows 10 Client, was aber meiner Erfahrung nach kaum zuverlässig nutzbar ist. Hatte das bei einigen Schulen so im Einsatz und alle 1-2 Jahre gabs wieder Rückmeldugnen das sich die PCs nicht mehr aktivieren. Grund war immer das der Windows 10 KMS-Server PC, nach der Installation eines der großen Windows 10 updates, einfach den KMS Dienst deinstalliert hat und man den nochmal komplett neu einrichten musste.

Deshalb musste man eigentlich, wenn man keine richtige Windos Domäne mit Windows Servern hatte. Auf den KMS Server verzichten oder halt unter Windows 10 mit größeren Einschränkungen legen.

## PY-KMS - Ein Linux KMS Server nachbau

Irgendwann haben dann ein paar schlaue Köpfe sich rangewagt und das KMS Server Protokal vom MS reverse engineered und damit auf Linux Portiert. Es gibt davon ein paar verschiedene Impelentationen, eine davon ist [PY-KMS](https://github.com/SystemRage/py-kms), die wie man vom Namen ableiten kann, in Python geschrieben wurde und wohl zu der verbreitesten gehört.
Das ganze könnte man jetzt wie auf github beschrieben von Hand installieren. Aber am einfachsten dürfte der Docker Container sein.

Einfach Docker auf einem Linux Server installieren (je nach Distribution) und dann mit dem Befehl installieren und einrichten: 

-----------------------------
docker run -d --name py-kms --restart always -p 1688:1688 pykmsorg/py-kms

-----------------------------


