+++
title = "About"
date = "2022-02-20"
author = "Daniel"
+++

# Wer ich bin und was das hier soll

Hallo zusammen,

ich bin Daniel, komme aus Deutschland und arbeite in der IT.

Die Seite hier soll eher eine kleine Sammlung von interresanten Projekten oder Programmen sein mit denen ich mich gerade beschäftige.

Alleine schon die Seite selber ist eigentlich so ein Projekt. Die Posts schreibe ich simple in [Markdown](https://de.wikipedia.org/wiki/Markdown) und mit [Hugo](https://gohugo.io/) wird das ganze dann in html zu einer Seite umgewandelt und dann per rsync auf den webserver hochgeladen.

Grüße,
Daniel
